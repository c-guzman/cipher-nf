/* Main Cipher-NF pipeline script
 * for single-stranded CHIP-seq data
 *
 * @authors
 * Carlos Guzman <cguzman.roma@gmail.com>
 */

// Default parameters
params.genomeSize = "hs"
params.config = "config.txt"
params.threads = 1
params.fragmentSize = 150
params.genome = "hg19"
params.gAnnotation = "ensembl"
params.rnaGenome = "hg19"
params.pvalue = 0.1
params.qvalue = 0.01
params.FDR = 1.0
params.chipQuality = 20
params.chipLength = 30

// Print Help

if (params.help && !params.mode) {
	log.info ''
	log.info 'C I P H E R - N F ~ version 2.0.0'
	log.info '================================='
	log.info 'Genomics Sequencing Pipeline'
	log.info 'Support: cguzman.roma@gmail.com'
	log.info ''
	log.info 'Usage: '
	log.info '     cipher.nf --mode --config --index [OPTIONS]...'
	log.info ''
	log.info '=====PIPELINE MODES:====='
	log.info '     --mode          Choose the type of sequencing pipeline to execute. (chip for ChIP-seq, rna for RNA-seq)'
	log.info ''
	log.info '=====Options: GENERAL====='
	log.info '     --help          Show this message and exits.'
	log.info '     --config        A tab separated file containing read information. (Default: config.txt)'
	log.info '     --index         A Bowtie2 or HISAT2 index directory.'
	log.info '     --threads       Allow N_THREADS per sample. (Default: 1)'
	log.info ''
	log.info '=====CHIP-SEQ OPTIONS:====='
	log.info 'Options: Trim Galore'
	log.info '     --trim          Preform trimming of reads and adapters.'
	log.info '     --chipQuality   The quality cutoff before a read is trimmed. (Default: 20)'
	log.info '     --chipLength    The length cutoff before a read is trimmed. (Default: 30)'
	log.info ''
	log.info 'Options: BOWTIE2'
	log.info ''
	log.info 'Options: BIGWIGS'
	log.info '     --fragmentSize  Size of fragment to extend reads. (Default: 150)'
	log.info ''
	log.info 'Options: PEAK CALLING'
	log.info '     --narrow        Call narrow peaks using MACS2.'
	log.info '     --broad         Call broad peaks using EPIC.'
	log.info ''
	log.info 'Options: MACS2'
	log.info '     --genomeSize    Select MACS2 genome size. (Default: hs)'
	log.info '     --qvalue        The q-value that should be used to call narrow peaks. (Default: 0.01)'
	log.info ''
	log.info 'Options: EPIC'
	log.info '     --genome        Select reference genome for Epic peak calling. (Default: hg19)'
	log.info '     --FDR           False Discovery Rate. Remove all islands with an FDR below cutoff. (Default: 1.0 - Finds all islands despite p-value)'
	log.info ''
	log.info 'Options: ANNOTATION'
	log.info '     --annotate      Annotate your peak files using HOMER.'
	log.info ''
	log.info '=====RNA-SEQ OPTIONS:====='
	log.info '     --transcriptome A transcriptome file in FASTA format.'
	log.info '     --experiment    A config file containing experimental information for DGE analysis.'
	log.info ''
	exit 1
}

//show chipseq help
if (params.help && params.mode == "chip") {
	log.info ''
	log.info '=====Options: GENERAL====='
	log.info '     --help          Show this message and exits.'
	log.info '     --config        A tab separated file containing read information. (Default: config.txt)'
	log.info '     --index         A Bowtie2 or HISAT2 index directory.'
	log.info '     --threads       Allow N_THREADS per sample. (Default: 1)'
	log.info ''
	log.info '=====CHIP-SEQ OPTIONS:====='
	log.info 'Options: Trim Galore'
	log.info '     --trim          Preform trimming of reads and adapters.'
	log.info '     --chipQuality   The quality cutoff before a read is trimmed. (Default: 20)'
	log.info '     --chipLength    The length cutoff before a read is trimmed. (Default: 30)'
	log.info ''
	log.info 'Options: BOWTIE2'
	log.info ''
	log.info 'Options: BIGWIGS'
	log.info '     --fragmentSize  Size of fragment to extend reads. (Default: 150)'
	log.info ''
	log.info 'Options: PEAK CALLING'
	log.info '     --narrow        Call narrow peaks using MACS2.'
	log.info '     --broad         Call broad peaks using EPIC.'
	log.info ''
	log.info 'Options: MACS2'
	log.info '     --genomeSize    Select MACS2 genome size. (Default: hs)'
	log.info '     --qvalue        The q-value that should be used to call narrow peaks. (Default: 0.01)'
	log.info ''
	log.info 'Options: EPIC'
	log.info '     --genome        Select reference genome for Epic peak calling. (Default: hg19)'
	log.info '     --FDR           False Discovery Rate. Remove all islands with an FDR below cutoff. (Default: 1.0 - Finds all islands despite p-value)'
	log.info ''
	log.info 'Options: ANNOTATION'
	log.info '     --annotate      Annotate your peak files using HOMER.'
	log.info ''
	exit 1
}

//show rnaseq help
if (params.help && params.mode == "rna") {
	log.info ''
	log.info '=====Options: GENERAL====='
	log.info '     --help          Show this message and exits.'
	log.info '     --config        A tab separated file containing read information. (Default: config.txt)'
	log.info '     --index         A Bowtie2 or HISAT2 index directory.'
	log.info '     --threads       Allow N_THREADS per sample. (Default: 1)'
	log.info ''
	log.info '=====RNA-SEQ OPTIONS:====='
	log.info '     --transcriptome A transcriptome file in FASTA format.'
	log.info '     --experiment    A config file containing experimental information for DGE analysis.'
	log.info ''
	exit 1
}

config_file = file(params.config)

// Check required parameters

if (!params.config) {
	exit 1, "Please specify a config file"
}

if (!params.index) {
	exit 1, "Please specify a bowtie2 index directory"
}

if (!params.mode) {
	exit 1, "Please specify a pipeline mode. Options are 'chip' for ChIP-seq and 'rna' for RNA-seq"
}

if (params.mode == 'chip') {
// Create fastq channel

fastqs = Channel
.from(config_file.readLines())
.map { line ->
	list = line.split()
	id = list[0]
	path = file(list[1])
	controlid = list[2]
	mark = list[3]
	[ id, path, controlid, mark ]
}

fastqs.into {
	fastqs1
	fastqs2
}

process pre_fastqc {

	publishDir 'results/chipseq/pretrim_fastqc', mode: 'copy'

	input:
	set id, file(fastq_file), controlid, mark from fastqs1

	output:
	set id, file("${id}_fastqc.html"), controlid, mark into pretrimfastqc

	script:
	"""
	fastqc ${fastq_file}
	"""
}

/*
 * Step 1. Trimming fastq files via trim_galore
 */

if (params.trim) {
process trimming {

	input:
	set id, file(fastq_file), controlid, mark from fastqs2

	output:
	set id, file("${id}_trimmed.fq.gz"), controlid, mark into trimmedfastqs1, trimmedfastqs2
	file "${id}_trimgalore_summary.txt"

	script:
	"""
	trim_galore -q ${params.chipQuality} --gzip --length ${params.chipLength} ${fastq_file} 2> ${id}_trimgalore_summary.txt
	"""
}

process post_fastqc {

	publishDir 'results/chipseq/posttrim_fastqc', mode: 'copy'

	input:
	set id, file(trimmedfastq_file), controlid, mark from trimmedfastqs1

	output:
	set id, file("${id}_trimmed_fastqc.html"), controlid, mark into posttrimmedfastqc

	script:
	"""
	fastqc ${trimmedfastq_file}
	"""
}
}

/*
 * Step 2. Map trimmed fastq files to bowtie2 index
 */

process mapping {

	input:
	set id, file(trimmed_fastq_file), controlid, mark from trimmedfastqs2

	output:
	set id, file("${id}.sam"), controlid, mark into sams
	file "${id}_alignment_summary.txt"

	script:
	"""
	bowtie2 -q --very-sensitive-local -t -p ${params.threads} -x ${params.index} -U ${trimmed_fastq_file} -S ${id}.sam 2> ${id}_alignment_summary.txt
	"""
}

/*
 * Step 3. Convert SAM to sorted BAM
 */

process sam2bam {

	input:
	set id, file(sam_file), controlid, mark from sams

	output:
	set id, file("${id}_sorted.bam"), controlid, mark into bams

	script:
	"""
	samtools view -b -F 3844 -@ ${params.threads} ${sam_file} | samtools sort -@ ${params.threads} -o ${id}_sorted.bam
	"""
}

process mark_duplicates {

	publishDir 'results/chipseq/bams', mode: 'copy'

	input:
	set id, file(bam_file), controlid, mark from bams

	output:
	set id, file("${id}_sorted.dupsmarked.bam"), controlid, mark into cleanbam1, cleanbam2, cleanbam3, cleanbam4, cleanbam5
	set id, file("${id}_marked_dup_metrics.txt"), controlid, mark into bammetrics

	script:
	"""
	picard MarkDuplicates REMOVE_DUPLICATES=true I=${bam_file} O=${id}_sorted.dupsmarked.bam M=${id}_marked_dup_metrics.txt
	"""
}


process bam_index {

	publishDir 'results/chipseq/bams', mode: 'copy'

	input:
	set id, file(bam1_file), controlid, mark from cleanbam1

	output:
	set id, file("${id}_sorted.dupsmarked.bam.bai"), controlid, mark into bams_index1, bams_index2

	script:
	"""
	sambamba index -t ${params.threads} ${bam1_file}
	"""
}

process deeptools_bigwig {

	publishDir 'results/chipseq/bigwigs', mode: 'copy'

	input:
	set id, file(bam2_file), controlid, mark from cleanbam2
	set id, file(bam_index), controlid, mark from bams_index1

	output:
	set id, file("${id}.RPKMnormalized.bigWig"), controlid, mark into bigwigs

	script:
	"""
	bamCoverage -b ${bam2_file} -o ${id}.RPKMnormalized.bigWig -bs 10 -p ${params.threads} --normalizeUsingRPKM -e ${params.fragmentSize}
	"""
}

process deeptools_bedgraph {

	publishDir 'results/chipseq/bedgraphs', mode: 'copy'

	input:
	set id, file(bam5_file), controlid, mark from cleanbam3
	set id, file(bam_index2), controlid, mark from bams_index2

	output:
	set id, file("${id}.RPKMnormalized.bedGraph"), controlid, mark into bedgraphs

	script:
	"""
	bamCoverage -b ${bam5_file} -o ${id}.RPKMnormalized.bedGraph -of=bedgraph -bs 10 -p ${params.threads} --normalizeUsingRPKM -e ${params.fragmentSize}
	"""
}
	
// seperate bams and inputs 
treat = Channel.create()
control = Channel.create()
cleanbam4.choice(treat, control) {
	it[3] == 'input' ? 1 : 0
}

// grab bams with no control
cleanbam5.tap { allBams }
.filter {
	it[2] == '-'
}.map {
	[it[0], it[1], it[2], it[3]]
}.tap { bamsNoInput }

// grab bams with controls
bamsWithInput = control.filter {
	it[2] != '-'
}
.cross(allBams) { it[2] }.map { c, t ->
	[t[0], t[1], c[1], t[3]]
}

bamsWithInput.into {
	bamsWithInput1
	bamsWithInput2
	bamsWithInput3
	bamsWithInput4
}

process bamToBed {

	publishDir 'results/chipseq/beds', mode: 'copy'

	input:
	set id, file(bam), file(control), mark from bamsWithInput4

	output:
	set id, file("${id}_treatment.bed.gz"), file("${id}_control.bed.gz"), mark into bamtobeds

	script:
	"""
	bamToBed -i ${bam} > ${id}_treatment.bed
	bamToBed -i ${control} > ${id}_control.bed
	gzip ${id}_treatment.bed
	gzip ${id}_control.bed
	"""
}

if (params.narrow) {
process call_narrowpeaks {

	publishDir 'results/chipseq/narrowpeaks', mode: 'copy'

	input:
	set id, file(bam), file(control), mark from bamsWithInput2

	output:
	set id, file("${id}_peaks.narrowPeak"), mark, val("narrowPeak") into narrowpeaks

	script:
	"""
	macs2 callpeak -t ${bam} -c ${control} -q ${params.qvalue} --keep-dup all -f BAM -g ${params.genomeSize} -n ${id}
	"""
}
}

if (params.broad) {
process call_broadpeaks {

	publishDir 'results/chipseq/broadpeaks', mode: 'copy'

	input:
	set id, file(bam_bed), file(control_bed), mark from bamtobeds

	output:
	set id, file("${id}_epic.bed"), mark, val("broadPeak") into broadpeaks

	script:
	"""
	epic --treatment ${bam_bed} --control ${control_bed} --genome ${params.genome} --fragment-size ${params.fragmentSize} -fdr ${params.FDR} > ${id}_epic.bed
	"""
}
}

//annotate narrow peaks
if (params.annotate && params.narrow) {
process annotate_narrowpeaks {

	publishDir 'results/chipseq/annotatedpeaks', mode: 'copy'

	input:
	set id, file(narrow), mark, val from narrowpeaks

	output:
	set id, file("${id}_narrow_annotatedPeaks.txt"), controlid, mark into narrowannotatedpeaks

	script:
	"""
	annotatePeaks.pl ${narrow} ${params.genome} > ${id}_narrow_annotatedPeaks.txt
	"""
}
}

//annotate broad peaks
if (params.annotate && params.broad) {
process annotate_broadpeaks {

	publishDir 'results/chipseq/annotatedpeaks', mode: 'copy'

	input:
	set id, file(broad), mark, val from broadpeaks

	output:
	set id, file("${id}_broad_annotatedPeaks.txt"), controlid, mark into broadannotatedpeaks

	script:
	"""
	annotatePeaks.pl ${broad} ${params.genome} > ${id}_broad_annotatedPeaks.txt
	"""
}
}
}

if (params.mode == 'rna') {

exp_file = file(params.experiment)
transcriptome_file = file(params.transcriptome)

// parse config file
fastqs = Channel
.from(config_file.readLines())
.map { line ->
	list = line.split()
	id = list[0]
	read1_path = file(list[1])
	read2_path = file(list[2])
	condition = list[3]
	[ id, read1_path, read2_path, condition ]
}

// seperate fastqs into two files
fastqs.into {
	fastqs1
	fastqs2
}

// pre-fastqc
process pre_fastqc {

	publishDir 'results/rnaseq/pretrim_fastqc', mode: 'copy'

	input:
	set id, file(read1_fastq_file), file(read2_fastq_file), condition from fastqs1

	output:
	set id, file("${id}_R1_fastqc.html"), file("${id}_R2_fastqc.html"), condition into pretrimfastqc

	script:
	"""
	fastqc -t 2 ${read1_fastq_file} ${read2_fastq_file}
	"""
}

// trim-galore to remove adapters
process trimming {

	input:
	set id, file(read1_fastq), file(read2_fastq), condition from fastqs2

	output:
	set id, file("${id}_R1_val_1.fq.gz"), file("${id}_R2_val_2.fq.gz"), condition into trimmedfastqs1, trimmedfastqs2

	script:
	"""
	trim_galore --no_report_file -q 30 --gzip --paired ${read1_fastq} ${read2_fastq}
	"""
}

//mapping with HISAT2
process mapping {

	publishDir 'results/rnaseq/bams', mode: 'copy'

	input:
	set id, file(read1_trimmed_fastq), file(read2_trimmed_fastq), condition from trimmedfastqs1

	output:
	set id, file("${id}_sorted.bam"), condition into bams, bams2, bams3

	script:
	"""
	hisat2 -t -p ${params.threads} -x ${params.index} -1 ${read1_trimmed_fastq} -2 ${read2_trimmed_fastq} --novel-splicesite-outfile ${id}_splice_sites.txt --un-conc-gz ${id}_un-conc.fastq.gz --al-conc-gz ${id}_al-conc.fastq.gz --met-file ${id}_metrics.txt 2> ${id}_align_summary.txt | samtools view -b - | samtools sort -@ ${params.threads} -O bam -o ${id}_sorted.bam
	"""
}

//index files with sambamba
process bam_index {

	publishDir 'results/rnaseq/bams', mode: 'copy'

	input:
	set id, file(bam_file), condition from bams

	output:
	set id, file("${id}_sorted.bam.bai"), condition into bamsIndex, bamsIndex2

	script:
	"""
	sambamba index -t ${params.threads} ${bam_file}
	"""
}

//generate forward normalized bigwig files for visualization
process deeptools_bigwig_forward {

	publishDir 'results/rnaseq/bigwigs', mode: 'copy'

	input:
	set id, file(bam_file), condition from bams2
	set id, file(bam_index), condition from bamsIndex

	output:
	set id, file("${id}.RPKMnormalized.forward.bigWig"), condition into forwardbigwigs

	script:
	"""
	bamCoverage -b ${bam_file} -o ${id}.RPKMnormalized.forward.bigWig -bs 10 -p ${params.threads} --normalizeUsingRPKM --filterRNAstrand forward
	"""
}

//generate reverse normalized bigwig files for visualization
process deeptools_bigwig_reverse {

	publishDir 'results/rnaseq/bigwigs', mode: 'copy'

	input:
	set id, file(bam_file), condition from bams3
	set id, file(bam_index), condition from bamsIndex2

	output:
	set id, file("${id}.RPKMnormalized.reverse.bigWig"), condition into reversebigwigs

	script:
	"""
	bamCoverage -b ${bam_file} -o ${id}.RPKMnormalized.reverse.bigWig -bs 10 -p ${params.threads} --normalizeUsingRPKM --filterRNAstrand reverse
	"""
}

// salmon index transcriptome
process salmon_index {

	publishDir 'results/rnaseq', mode: 'copy'

	input:
	file(transcriptome_file)

	output:
	file("transcriptome.index") into transcriptome_index

	script:
	"""
	salmon --no-version-check index -t ${transcriptome_file} -p ${params.threads} -i transcriptome.index
	"""
}

//use salmon to psuedo-align for DGE
process salmon_quant {

	publishDir 'results/rnaseq/dge', mode: 'copy', overwrite: 'true'

	input:
	set id, file(read1_trimmed_fastq), file(read2_trimmed_fastq), condition from trimmedfastqs2
	file transcriptome_index from transcriptome_index.first()

	output:
	file "salmon_${id}" into salmon_out_dirs

	script:
	"""
	mkdir salmon_${id}
	salmon --no-version-check quant -i transcriptome.index -l IU -1 <(gunzip -c ${read1_trimmed_fastq}) -2 <(gunzip -c ${read2_trimmed_fastq}) -p ${params.threads} -o salmon_${id}
	"""
}

//conduct DGE analysis using DESeq2

process differential_gene_expression {

	input:
	file 'salmon/*' from salmon_out_dirs.toSortedList()
	file exp_file

	output:
	file "rnaseq_dge_report.html"

	script:
	"""
	Rscript '$baseDir/bin/dge.R' salmon ${exp_file}
	"""
}
}

workflow.onComplete {
    println ""
    println "Workflow completed on: $workflow.complete"
    println "Execution status: ${ workflow.success ? 'Succeeded' : 'Failed' }"
    println "Workflow Duration: $workflow.duration"
    println ""
}

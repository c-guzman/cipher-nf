#!/bin/bash

conda install -c bioconda deeptools samtools bowtie2 hisat2 macs2 bioepic nextflow sambamba trim-galore fastqc picard salmon

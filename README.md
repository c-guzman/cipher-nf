# <p align="center"> C I P H E R </p>
CIPHER is a ChIP-seq processing pipeline written in Nextflow for single-stranded data. To get more information on parameters open up a terminal and type in `nextflow run cipher-nf --help`.

## Help

```
==============================
            C I P H E R          
==============================
Chromatin Immunoprecipitation (ChIP) Processing Pipeline using Nextflow

Run: 
    cipher-nf.nf --config --index [OPTIONS]...

General Options: 
    --help               Show this message and exit.
    --config             A TAB seperated file containing metadata information.
    --index              An appropriate index or genome fasta file for chosen aligner.
    --threads            The number of threads cipher should use. (Default: 1)
    -qs                  The number of processes that can be parallelized at one time. (Default: 1 per sample)

BigWig Options: 
    --fragmentSize       Extends reads to fragment size. (Default: 150)

Peak Calling Options:
    --narrow		 Call narrow peaks using MACS2.
    --broad		 Call broad peaks using EPIC.

MACS2 Options: 
    --genomeSize         The genome size of your reference species as indicated in the MACS2 manual. (Default: hs)

Epic Options: 
    --genome             The genome to analyze as indicated in Epic manual. (Default: hg19)

```

## Running CIPHER

CIPHER is hosted on GitLab and can be cloned easily onto your local computer via the following command:

`git clone git@gitlab.com:c-guzman/cipher-nf.git`

## Mapping

CIPHER maps ChIP-seq reads using Bowtie2's `--very-sensitive-local` option.

## Input

The input data and metadata should be specified in a config file passed via the `--config` parameter. Here is an example of the file format:

```
sample1     /path/to/sample1.fastq.gz   control1    Pol-II
sample2     /path/to/sample2.fastq.gz   control1    Pol-II
sample3     /path/to/sample3.fastq.gz   control2    H3K4me3
control1    /path/to/control1.fastq.gz  control1    input
control2    /path/to/control2.fastq.gz  control2    input
```

Each column in the file corresponds to:

1. **SampleID**: The prefix of your file. Typically the name of your mark. Must be the same as the prefix of your .fastq.gz file.
2. **Path**: The path to the fastq file to be processed.
3. **ControlID**: The prefix of the Input file to be used for peak calling. Must be the same as the control sampleID. `-` if there is no control.
4. **Mark**: The mark/histone being processed or `input` if the line refers to a control.

**NOTE:** CIPHER does not merge bam files, so if you would like to merge bam files you will have to do so manually. You can however, merge all fastq files for the same sample (assuming they are not pair-ended) using the `cat` function, and then feed that merged fastq file into CIPHER.

## Output

Output BAM files were created using `samtools v1.3.1` and the `-F3844` parameter discarding unmapped reads. Index files were created using `sambamba index`.

CIPHER will output the following directories in a main 'results' folder:

* annotatedpeaks
* bams
* bed
* bedgraphs
* bigwigs
* broadpeaks
* narrowpeaks
* pretrim_fastqc
* posttrim_fastqc


## Dependencies

There are several dependencies required for running CIPHER. Older versions of software may work, but cannot be guaranteed.

* Nextflow (Version 0.19.3+)
* Samtools (Version 1.3.1+)
* MACS2 (Version 2.1.0+)
* Bowtie2 (Version 2.2.8+)
* Deeptools (Version 2.2.4+)
* Epic (Version 0.1.2+)
* Sambamba (Version 0.6.3+)
* HOMER (Version 4.8+)

For convinence a python script is included in order to download all required conda packages. It can be run by using the following:

`python requirements.py`

In order to estimate fragment size and gain other quality control information about your aligned reads, we have provided a python script named `fragment_size.py`. This script simply wraps around HOMER's `makeTagDirectory` function, and thus must have the HOMER suite installed to work properly.


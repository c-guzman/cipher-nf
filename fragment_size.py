import argparse
import subprocess

parser = argparse.ArgumentParser(description="A python script to identify fragment size of your sample")
parser.add_argument("-i", "--input", required=True, help="Input BAM file")
parser.add_argument("-o", "--outputDir", required=True, help="Output Directory")
parser.parse_args()

## Global variables
args = parser.parse_args()
input = args.input
outputDir = args.outputDir

## Run subprocess
homer_command = "makeTagDirectory {} {}".format(outputDir, input)
subprocess.check_output(['bash', '-c', homer_command])
